import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Required components for which route services to be activated
import { SignInComponent } from './core/components/sign-in/sign-in.component';
import { SignUpComponent } from './core/components/sign-up/sign-up.component';
import { DashboardComponent } from './core/components/dashboard/dashboard.component';
import { ForgotPasswordComponent } from './core/components/forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './core/components/verify-email/verify-email.component';
// Import canActivate guard services
import { AuthGuard } from './core/guards/auth.guard';
import { LogoutComponent } from './core/components/logout/logout.component';
import { SecureInnerPagesGuard } from './core/guards/secure-inner-pages.guard';

// Include route guard in routes array
const routes: Routes = [
  { path: '', redirectTo: '/sign-in', pathMatch: 'full' },
  { path: 'sign-in', component: SignInComponent, canActivate: [SecureInnerPagesGuard] },
  { path: 'logout', component: LogoutComponent, canActivate: [AuthGuard] },
  { path: 'register-user', component: SignUpComponent, canActivate: [SecureInnerPagesGuard] },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'forgot-password', component: ForgotPasswordComponent, canActivate: [] },
  { path: 'verify-email-address', component: VerifyEmailComponent, canActivate: [] },
  { path: 'home', loadChildren: () => import('./feature/home/home-routing.module').then(m => m.HomeRoutingModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
