import { FirestoreEntity } from '../../shared/model/firestore.entity';

export class User implements FirestoreEntity {
   id?: string;
   uid: string;
   email: string;
   displayName: string;
   photoURL: string;
   emailVerified: boolean;
   creationDate?: Date;
}
