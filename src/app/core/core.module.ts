import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// Reactive Form
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// App routing modules
// App components
// Firebase services + enviorment module
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../../environments/environment';
import { LayoutComponent } from './layout/layout.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './components/verify-email/verify-email.component';
import { RouterModule } from '@angular/router';
import { MenuComponent } from './components/menu/menu.component';
import { SharedModule } from '../shared/shared.module';
import { LogoutComponent } from './components/logout/logout.component';

// Auth service


@NgModule({
    declarations: [
        SignInComponent,
        SignUpComponent,
        DashboardComponent,
        ForgotPasswordComponent,
        VerifyEmailComponent,
        LayoutComponent,
        MenuComponent,
        LogoutComponent
    ],
    imports: [
        BrowserModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
        ReactiveFormsModule,
        RouterModule,
        SharedModule,
        FormsModule
    ],
    exports: [
        SignInComponent,
        SignUpComponent,
        DashboardComponent,
        ForgotPasswordComponent,
        VerifyEmailComponent,
        LayoutComponent
    ]
})

export class CoreModule {
}
