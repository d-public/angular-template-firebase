import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from '../../models/user';
import { map, switchMap } from 'rxjs/operators';


@Component({
    selector: 'core-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    user: User;

    constructor(
        public authService: AuthService,
        private afs: AngularFirestore
    ) {
    }

    ngOnInit() {
        this.authService.userData$.pipe(
            switchMap(user => {
                return this.afs.doc(`users/${user.uid}`).get();
            }),
            map(user => user.data())
        ).subscribe(user => {
            this.user = {
                uid: user.uid,
                displayName: user.displayName,
                email: user.email,
                emailVerified: user.emailVerified,
                photoURL: user.photoURL
            };
        });
    }

    updateUser() {
        console.log(this.user);
        this.authService.SetUserData(this.user);
    }
}
