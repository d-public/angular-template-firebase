import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
    selector: 'core-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

    isLoggedIn = false;
    isMobileMenuExpanded: boolean = false;

    constructor(public authService: AuthService) {
    }

    ngOnInit() {
        this.authService.isLoggedIn$.subscribe(isLoggedIn => {
            this.isLoggedIn = isLoggedIn;
        });
    }

    toggleMobileMenu() :void {
        this.isMobileMenuExpanded = !this.isMobileMenuExpanded;
    }
}
