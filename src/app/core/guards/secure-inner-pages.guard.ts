import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class SecureInnerPagesGuard implements CanActivate {

    constructor(
        public authService: AuthService,
        public router: Router
    ) {
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean | UrlTree {
        return this.authService.isLoggedIn$.pipe(
            map(isLoggedIn => {
                console.log('inner-page: ', isLoggedIn);
                if (isLoggedIn) {
                    window.alert('You are not allowed to access this URL!');
                    this.router.navigate(['dashboard']);
                    return false;
                }
                return true;
            })
        );
    }

}
