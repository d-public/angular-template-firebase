import { NgModule } from '@angular/core';
// Reactive Form
// App routing modules
// App components
// Firebase services + enviorment module
import { AuthenticatedClickDirective } from './directive/authenticated-click.directive';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { AuthenticatedViewDirective } from './directive/authenticated-view.directive';
import { IsOwnerDirective } from './directive/is-owner.directive';

// Auth service


@NgModule({
    declarations: [
        AuthenticatedClickDirective,
        AuthenticatedViewDirective,
        IsOwnerDirective
    ],
    imports: [
        ToastModule
    ],
    exports: [
        AuthenticatedClickDirective,
        AuthenticatedViewDirective,
        IsOwnerDirective
    ],
    providers: [MessageService]
})

export class SharedModule {
}
