export interface FirestoreEntity {
    id?: string; // Optional for new entities
    creationDate?: Date;
    authorId?: string;
}
