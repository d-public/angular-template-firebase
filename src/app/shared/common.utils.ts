export function firebaseSerialize<T>(object: T) {
    return JSON.parse(JSON.stringify(object));
}
