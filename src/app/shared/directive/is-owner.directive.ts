import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';
import { AuthService } from '../../core/services/auth.service';
import { filter } from 'rxjs/operators';

@Directive({
    selector: '[isOwner]'
})
export class IsOwnerDirective implements OnInit {

    @Input()
    isOwner: string;

    constructor(private elr: ElementRef, private renderer: Renderer2, private userDetailService: AuthService) {
    }

    ngOnInit(): void {
        this.userDetailService.userData$.pipe(
            filter(user => !!user)
        ).subscribe(user => {
            if (user.uid !== this.isOwner) {
                const el = this.elr.nativeElement;
                el.parentNode.removeChild(el);
            }
        });
    }
}
