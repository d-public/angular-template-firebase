import { Directive, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { AuthService } from '../../core/services/auth.service';

@Directive({
    selector: '[authenticatedClick]'
})
export class AuthenticatedClickDirective implements OnInit {

    @Output()
    authenticatedClick = new EventEmitter<MouseEvent>();
    @Input()
    title: string;
    @Input()
    content: any;

    constructor(private elr: ElementRef, private renderer: Renderer2, private userDetailService: AuthService) {
    }

    ngOnInit(): void {}

    @HostListener('click', ['$event'])
    onclick(event: MouseEvent) {
        this.userDetailService.isLoggedIn$.subscribe(isLoggedIn => {
            console.log('logged? ', isLoggedIn);
            if (isLoggedIn) {
                this.authenticatedClick.emit(event);
            } else {
                // this.guestUserWarningModalUxService.add({ severity: 'danger', summary: this.title, detail: this.content });
            }
        });
    }
}
