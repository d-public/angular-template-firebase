import { Directive, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { AuthService } from '../../core/services/auth.service';

@Directive({
    selector: '[authenticatedOnly]'
})
export class AuthenticatedViewDirective implements OnInit {

    constructor(private elr: ElementRef, private renderer: Renderer2, private userDetailService: AuthService) {
    }

    ngOnInit(): void {
        this.userDetailService.isLoggedIn$.subscribe(isLoggedIn => {
            if (!isLoggedIn) {
                const el = this.elr.nativeElement;
                el.parentNode.removeChild(el);
            }
        });
    }
}
