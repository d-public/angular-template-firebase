import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { from, Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { firebaseSerialize } from '../common.utils';
import { FirestoreEntity } from '../model/firestore.entity';
import { User } from '../../core/models/user';
import { AuthService } from '../../core/services/auth.service';

// We need a function that will turn our JS Objects into an Object
// that Firestore can work with

// We need a base Entity interface that our models will extend


export class FirestoreCrudService<T extends FirestoreEntity> {
    // Reference to the Collection in Firestore
    protected collection: AngularFirestoreCollection<T>;

    /* We need to ask for the AngularFirestore Injectable
     * and a Collection Name to use in Firestore
     */
    constructor(protected afs: AngularFirestore, public collectionName: string, protected authService: AuthService) {
        // We then create the reference to this Collection
        this.collection = this.afs.collection(collectionName);
    }

    /**
     * We look for the Entity we want to add as well
     * as an Optional Id, which will allow us to set
     * the Entity into a specific Document in the Collection
     */
    protected add(entity: T, id?: string): Promise<T> {
        // We want to create a Typed Return of the added Entity
        return new Promise<T>((resolve, reject) => {
            if (id) {
                // If there is an ID Provided, lets specifically set the Document
                this.collection
                    .doc(id)
                    .set(firebaseSerialize<T>(entity))
                    .then(ref => {
                        resolve(entity);
                    });
            } else {
                // If no ID is set, allow Firestore to Auto-Generate one
                this.collection.add(firebaseSerialize<T>(entity)).then(ref => {
                    // Let's make sure we return the newly added ID with Model
                    const newentity = {
                        id: ref.id,
                        ...entity,
                    };
                    resolve(newentity);
                });
            }
        });
    }

    /**
     * Our get method will fetch a single Entity by it's Document ID
     */
    get$(id: string): Observable<T> {
        return this.collection
            .doc<T>(id)
            .snapshotChanges()
            .pipe(
                // We want to map the document into a Typed JS Object
                map(doc => {
                    // Only if the entity exists should we build an object out of it
                    if (doc.payload.exists) {
                        const data = doc.payload.data() as T;
                        const payloadId = doc.payload.id;
                        return { id: payloadId, ...data };
                    }
                })
            );
    }

    /*
     * Our list method will get all the Entities in the Collection
     */
    list$(): Observable<T[]> {
        return this.collection.snapshotChanges().pipe(
            // Again we want to build a Typed JS Object from the Document
            map(changes => {
                return changes.map(a => {
                    const data = a.payload.doc.data() as T;
                    data.id = a.payload.doc.id;
                    return data;
                });
            })
        );
    }

    /* Our Update method takes the full updated Entity
     * Including it's ID property which it will use to find the
     * Document. This is a Hard Update.
     */
    protected update(entity: T): Promise<T> {
        return new Promise<T>((resolve, reject) => {
            this.collection
                .doc<T>(entity.id as string)
                .set(firebaseSerialize<T>(entity))
                .then(() => {
                    resolve({
                        ...entity,
                    });
                });
        });
    }

    delete$(id: string): Observable<void> {
        return from(new Promise<void>((resolve, reject) => {
            this.collection
                .doc<T>(id)
                .delete()
                .then(() => {
                    resolve();
                });
        }));
    }

    protected create(unit: T): Observable<T> {
        unit.id = this.afs.createId();
        unit.creationDate = new Date();
        return this.authService.userData$.pipe(
            switchMap((user: User) => {
                unit.authorId = user.uid;
                return this.add(unit);
            })
        );
    }
}
