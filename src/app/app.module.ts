import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// Reactive Form
import { ReactiveFormsModule } from '@angular/forms';
// App routing modules
import { AppRoutingModule } from './app-routing.module';
// App components
import { AppComponent } from './app.component';
// Auth service
import { CoreModule } from './core/core.module';
import { HomeModule } from './feature/home/home.module';


@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        ReactiveFormsModule,
        CoreModule,
        HomeModule
    ],
    bootstrap: [AppComponent]
})

export class AppModule {
}
