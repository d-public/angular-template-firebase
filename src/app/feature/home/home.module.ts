import { NgModule } from '@angular/core';
// Home routing modules
import { HomeRoutingModule } from './home-routing.module';
// Home components
import { HomeComponent } from './home.component';
import { CommonModule } from '@angular/common';
import { Test1Component } from './test1/test1.component';
import { Test2Component } from './test2/test2.component';


@NgModule({
    declarations: [
        HomeComponent,
        Test1Component,
        Test2Component,
    ],
    exports: [HomeComponent],
    imports: [
        HomeRoutingModule,
        CommonModule
    ]
})

export class HomeModule {
}
