import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Required components for which route services to be activated
// Import canActivate guard services
import { HomeComponent } from './home.component';
import { Test1Component } from './test1/test1.component';
import { Test2Component } from './test2/test2.component';

// Include route guard in routes array
const routes: Routes = [
    {
        path: '', component: HomeComponent, children: [
            { path: 'test1', component: Test1Component },
            { path: 'test2', component: Test2Component }
        ]
    }, //, canActivate: [SecureInnerPagesGuard] }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class HomeRoutingModule {
}
