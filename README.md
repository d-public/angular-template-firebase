# Angular Firebase Template

This project has been created & improved for my need based on [this article](https://dev.to/coly010/building-a-firebase-crud-service-for-angular-2629) from [dev.to](https://dev.to) 

## Content
I needed a template for Angular that allows me to quickly create a web application with authentication with firebase.

## Structure
```
.

├── node_modules
│   └── ...
└── src
    ├── app
    │   ├── core
    │   ├── feature
    │   └── shared
    ├── assets
    │   └── i18n
    └── environments
```
The application is divided in 3 main forlders:
- *core*: It contains the core logic of the application, \
e.g: the authentication, the layout, the guard, the profile management
- *feature*: It contains all the feature needed that will be lazy loaded
- *shared*: It contains all the shared code between the features and the core application

In the *assets/i18n* folder, you'll find the translation file for the application.

The folder *environments* it's where you'll put the configuration for firebase. 

## Package & Tools

### Functionnalities
- firebase
- ngrx
- primeng
- ngx translate

### CSS
- bulma
- prime-icons
- flag-icon-css
- font awesome

### Code Quality
- SonarQube
